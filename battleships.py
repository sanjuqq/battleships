import random
import time
import filehandler

f = filehandler

board = [[]]
board_size = 10
amountOfShips = 2
hits_left = 50
game_over = False
amountOfShips_sunk = 0
ship_placement = [[]]
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def valid_placement(start_x, end_x, start_y, end_y):
    global board
    global ship_placement

    all_valid = True
    for r in range(start_x, end_x):
        for c in range(start_y, end_y):
            if board[r][c] != ".":
                all_valid = False
                break
    if all_valid:
        ship_placement.append([start_x, end_x, start_y, end_y])
        for r in range(start_x, end_x):
            for c in range(start_y, end_y):
                board[r][c] = "O"
    return all_valid


def try_placement(x_axis, y_axis, direction, length):
    global board_size

    start_x, end_x, start_y, end_y = x_axis, x_axis + 1, y_axis, y_axis + 1
    if direction == "left":
        if y_axis - length < 0:
            return False
        start_y = y_axis - length + 1

    elif direction == "right":
        if y_axis + length >= board_size:
            return False
        end_y = y_axis + length

    elif direction == "up":
        if x_axis - length < 0:
            return False
        start_x = x_axis - length + 1

    elif direction == "down":
        if x_axis + length >= board_size:
            return False
        end_x = x_axis + length

    return valid_placement(start_x, end_x, start_y, end_y)


def create_board():
    global board
    global board_size
    global amountOfShips
    global ship_placement

    random.seed(time.time())

    rows, cols = (board_size, board_size)

    board = []
    for r in range(rows):
        x_axis = []
        for c in range(cols):
            x_axis.append(".")
        board.append(x_axis)

    amountOfShips_placed = 0

    ship_placement = []

    while amountOfShips_placed != amountOfShips:
        random_x_axis = random.randint(0, rows - 1)
        random_y_axis = random.randint(0, cols - 1)
        direction = random.choice(["left", "right", "up", "down"])
        ship_size = random.randint(3, 5)
        if try_placement(random_x_axis, random_y_axis, direction, ship_size):
            amountOfShips_placed += 1


def print_board():
    global board
    global alphabet

    debug_mode = True

    alphabet = alphabet[0: len(board) + 1] ## Slicing so it fits the board

    for x_axis in range(len(board)):
        print(alphabet[x_axis], end=") ")
        for y_axis in range(len(board[x_axis])):
            if board[x_axis][y_axis] == "O":
                if debug_mode:
                    print("O", end=" ") ## Change the "O" to "." to hide the ships!
                else:
                    print(".", end=" ")
            else:
                print(board[x_axis][y_axis], end=" ")
        print("")

    print("  ", end=" ")
    for i in range(len(board[0])):
        print(str(i), end=" ")
    print("")


def valid_hit_placement():
    global alphabet
    global board

    is_valid_placement = False
    x_axis = -1
    y_axis = -1
    while is_valid_placement is False:
        placement = input("Enter x axis (A-J) and y axis (0-9) such as A3: ")
        placement = placement.upper()
        if len(placement) <= 0 or len(placement) > 2:
            print("Error: Please enter only one x axis and y axis such as A3")
            continue
        x_axis = placement[0]
        y_axis = placement[1]
        if not x_axis.isalpha() or not y_axis.isnumeric():
            print("Error: Please enter letter (A-J) for x axis and (0-9) for y axis")
            continue
        x_axis = alphabet.find(x_axis)
        if not (-1 < x_axis < board_size):
            print("Error: Please enter letter (A-J) for x axis and (0-9) for y axis")
            continue
        y_axis = int(y_axis)
        if not (-1 < y_axis < board_size):
            print("Error: Please enter letter (A-J) for x axis and (0-9) for y axis")
            continue
        if board[x_axis][y_axis] == "#" or board[x_axis][y_axis] == "X":
            print("You have already hit this position")
            continue
        if board[x_axis][y_axis] == "." or board[x_axis][y_axis] == "O":
            is_valid_placement = True

    return x_axis, y_axis


def check_for_ship_sunk(x_axis, y_axis):
    global board

    for placement in ship_placement:
        start_x = placement[0]
        end_x = placement[1]
        start_y = placement[2]
        end_y = placement[3]
        if start_x <= x_axis <= end_x and start_y <= y_axis <= end_y:
            # Ship found, now check if its all sunk
            for r in range(start_x, end_x):
                for c in range(start_y, end_y):
                    if board[r][c] != "X":
                        return False
    return True


def shoot_bullet():
    global board
    global amountOfShips_sunk
    global hits_left

    x_axis, y_axis = valid_hit_placement()
    print("")
    print("-------------------------")

    if board[x_axis][y_axis] == ".":
        print("You missed, unlucky!")
        board[x_axis][y_axis] = "#"
    elif board[x_axis][y_axis] == "O":
        print("You hit something", end=" ")
        board[x_axis][y_axis] = "X"
        if check_for_ship_sunk(x_axis, y_axis):
            print("A ship sunk.")
            amountOfShips_sunk += 1
        else:
            print("Ship got hit, lucky!")
    f.updateHits()
    hits_left -= 1


def check_for_game_over():
    global amountOfShips_sunk
    global amountOfShips
    global hits_left
    global game_over

    if amountOfShips == amountOfShips_sunk:
        print("You won :)")
        f.updateWins()
        game_over = True
    elif hits_left <= 0:
        print("You lost, noob.")
        f.updateLosses()
        game_over = True


def main():
    global game_over

    print("-----Battleships initializing-----")
    print("You have 50 hits to take down the ships.")

    create_board()

    while game_over is False:
        print_board()
        print("Amount of ships remaining: " + str(amountOfShips - amountOfShips_sunk))
        print("Amount of hits left: " + str(hits_left))
        shoot_bullet()
        print("------------------------")
        print("")
        check_for_game_over()

