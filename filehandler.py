import json


def printstats():
    with open('stats.json', 'r') as myfile:
        data=myfile.read()
    obj = json.loads(data)
    print("Wins: " + str(obj['Wins']))
    print("Losses: " + str(obj['Losses']))
    print("Bullets used: " + str(obj['bulletsUsed']))


def updateWins():
    with open('stats.json') as myreadfile:
        readData=json.load(myreadfile)
    readData['Wins'] += 1
    
    with open('stats.json', 'w') as mywritefile:
        json.dump(readData, mywritefile, indent=4)


def updateLosses():
    with open('stats.json') as myreadfile:
        readData=json.load(myreadfile)
    readData['Losses'] += 1
    
    with open('stats.json', 'w') as mywritefile:
        json.dump(readData, mywritefile, indent=4)


def updateHits():
    with open('stats.json') as myreadfile:
        readData=json.load(myreadfile)
    readData['bulletsUsed'] += 1
    
    with open('stats.json', 'w') as mywritefile:
        json.dump(readData, mywritefile, indent=4)


