## Description.
1. A 10x10 board will have ships of variable length randomly placed
2. You will have 50 bullets to take down the ships that are placed down
3. You can choose an x axis and a y axis such as A1 to shoot at
4. You will see every shot that misses and hits
5. A ship cannot be placed diagonally.
6. You wil by eliminating all the ships before your bullets run out.
7. "." Is water.
8. "O" = Ship.
9. "X" = Ship that has been hit.
10. "#" Missed hit.
## Version.

Python v 3.9.7

## Sources.

https://docs.python.org/3/library/json.html

## Developer.

https://gitlab.com/sanjuqq

