import sys
import time
import os
import battleships
import filehandler
f = filehandler


def clear_screen():
    os.system('cls' if os.name=='nt' else 'clear')

def welcomeMenu():
    print("******************************\n" "Welcome to my Battleships game!" "\n******************************")
    time.sleep(1)
    clear_screen()
    print('__________         __    __  .__                .__    .__              ')
    print('\______   \_____ _/  |__/  |_|  |   ____   _____|  |__ |__|_____  ______')
    print(' |    |  _/\__  \\   __\   __\  | _/ __ \ /  ___/  |  \|  \____ \/  ___/')
    print(' |    |   \ / __ \|  |  |  | |  |_\  ___/ \___ \|   Y  \  |  |_> >___ \ ')
    print(' |______  /(____  /__|  |__| |____/\___  >____  >___|  /__|   __/____  >')
    print('       \/      \/                     \/     \/     \/   |__|       \/ ')
    time.sleep(1)
    print("____________________________________________________")
    print('This is a battleships game, try to win!')
    time.sleep(0.5)
    print('**** Developers: Sanju - https://gitlab.com/sanjuqq')
    print('____________________________________________________')
    time.sleep(0.5)
    input("Press any key to start the game")
    Welcome()
    return

def finish():
    sys.exit()

def Welcome():
    clear_screen()
    print("\nBattleships\nPlease choose one of the following \n"  " 1. Start a game\
        \n 2. Show past results" "\n 3. Close program \n")
    while True:
        choice = input("Your choice: ")
        if choice == "1":
            battleships.main()
       
        elif choice == "2":
            f.printstats()
        
        elif choice == "3":
            finish()
        else:
            print("\nWrong number. Choose one between 1-3")
            input("Press any key to continue")



welcomeMenu()

